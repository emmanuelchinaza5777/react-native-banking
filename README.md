# React-Native Banking
**Developer: CHUKWUNAZAEKPERE OBIOMA**.

**Summary**: This is a mobile - banking app, built with a state management library: Redux and React-Native. 

**Expo URI**: https://exp.host/@chukwunazaekpere/uranium_bank

**Procedures for use**: in order to use the app, heed to the following:

a) From play - store, download the expo app on your phone.

b) Follow the "Expo URI" link above (with a notebook).

c) A QR bar - code is shown on the right of your screen.

d) Scan the code and enjoy! (More instrucutional tips on the page)


**Description**: When a user comes to the splash - screen, there's a 'sign up' and a 'login' button. 
If a user hits the login screen without signinig up, a validation is made on the global state to check if they're signed up.
If they aren't, access is denied and they're redirected to the 'sign up' page. On signinig up, a user is required to
enter; firstname, lastname, email password, confirm - password and phone.
 
A validation is done to check the inputted phone - "phone: ensure it's upto eleven and has not been used by another user."
if validation rule passes, an activity - indicator spins up for 7secs and thereafter,  the user is taken to his/her dashboard.

At the dashboard, a user can; make transfer, deposit funds, check balance and see all transactions.
If a user wishes to make transfer, the make transfer screen is composed of bottom - tabs for making transfer and seeing all recent transfer ever done. The make deposit screen follows similar configuration as the make transfer screen.
